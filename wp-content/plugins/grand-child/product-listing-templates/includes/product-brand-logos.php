<?php
$collection =  @$meta_values['collection'][0];
$storename = do_shortcode('[Retailer "companyname"]');
$alt = "Floorte waterproof hardwood flooring for home | ".$storename;
$brand = trim(@$meta_values['brand_facet'][0]);
if ( $brand == 'Shaw Floors') { 
	if($collection == 'Floorte Exquisite' || $collection == 'Floorte Magnificent'){	?>
		<span class="floorte_brand">SHAW FLOORS</span>
		<img class="floorte_brandlogo" src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/floorte_waterproof.png" alt="floorte" class="product-logo" />
 <?php }else{
	?>
	<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/shaw_logo.png" alt="Shaw Floors" class="product-logo" />
<?php } } elseif ($brand == 'Anderson Tuftex') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/anderson_tuftex_logo.png" alt="Anderson Tuftex" class="product-logo" />
<?php } elseif ($brand == 'Karastan'){ ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/karastan_logo.png" alt="Karastan" class="product-logo" />
<?php }elseif ($brand == 'Armstrong') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/armstrong_logo.png" alt="Armstrong" class="product-logo" />
<?php } elseif ($brand == 'Dream Weaver') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/dreamweaver_logo.png" alt="Dream Weaver" class="product-logo" />
<?php }elseif ($brand == 'Philadelphia Commercial') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/philadelphia_commercial_logo.png" alt="Philadelphia Commercial" class="product-logo" />
<?php }elseif ($brand == 'COREtec') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/coretec_logo.png" alt="COREtec" class="product-logo" />
<?php }  elseif ($brand == 'Daltile') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/daltile_logo.png" alt="Daltile" class="product-logo" />
<?php } elseif ($brand == 'Floorscapes') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/floorscapes.jpg" alt="Floorscapes" class="product-logo" />
<?php }  elseif ($brand == 'Mohawk') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mohawk_logo.png" alt="Mohawk" class="product-logo" />
<?php } elseif ($brand == 'Bruce') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/bruce_logo.png" alt="Bruce" class="product-logo" />
<?php } elseif ($brand == 'American Olean') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/amrican_olean_logo.jpg" alt="American Olean" class="product-logo" />
<?php } elseif ($brand == 'Mannington') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mannington_logo.png" alt="Mannington" class="product-logo" />
<?php } elseif ($brand == 'Fabrica') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/fabrica.png" alt="Fabrica" class="product-logo" />
<?php } elseif ($brand == 'Masland') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mas-land.png" alt="Masland" class="product-logo" />
<?php } elseif ($brand == 'Dixie Home') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/dixie-home.png" alt="Dixie Home" class="product-logo" />
<?php } elseif ($brand == 'Nourison') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/nourison_logo.png" alt="Nourison" class="product-logo" />	
<?php } elseif ($brand == 'Hydraluxe') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/hydraluxe_logo.png" alt="Hydraluxe" class="product-logo" />	
<?php } elseif ($brand == 'Karndean') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/karndean_logo.jpg" alt="Karndean" class="product-logo" />		
<?php } elseif ($brand == 'Cali') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/cali_logo.jpg" alt="Cali" class="product-logo" />				
<?php }elseif ($brand == 'Robbins') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/robbins.png" alt="Robbins" class="product-logo" />	
<?php }elseif ($brand == 'Mercier') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/mercier.jpg" alt="Mercier" class="product-logo" />
<?php }elseif ($brand == 'Hallmark') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/hallmark.jpg" alt="Hallmark" class="product-logo" />			
<?php }elseif ($brand == 'Bella Cera') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/bella_cera.jpg" alt="Bella Cera" class="product-logo" />
<?php }elseif ($brand == 'Godfrey Hirst') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/godfrey_hirst.jpg" alt="Godfrey Hirst" class="product-logo" />	
<?php }elseif ($brand == 'Stanton') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/stanton.jpg" alt="Stanton" class="product-logo" />
<?php }elseif ($brand == 'Emser') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/emsertile_logo.png" alt="Emser" class="product-logo" />
<?php }elseif ($brand == 'Marazzi') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/marazzi.png" alt="Marazzi" class="product-logo" />
<?php }elseif ($brand == 'Happy Feet') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/happy_feet.png" alt="Happy Feet" class="product-logo" />
<?php }elseif ($brand == 'Happy Floors') { ?>
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/happy_floors.png" alt="Happy Floors" class="product-logo" />	
<?php }elseif ($brand == 'Somerset') { ?>	
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/somerset.jpg" alt="Somerset" class="product-logo" />	
<?php }elseif ($brand == 'Naturally Aged Flooring') { ?>	
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/naturally_aged_flooring.png" alt="Somerset" class="product-logo" />
<?php }elseif ($brand == 'MSI Tile') { ?>	
		<img src="<?php echo plugin_dir_url( __FILE__ ); ?>brand_logo/msi_tile.png" alt="MSI Tile" class="product-logo" />							
<?php }	else { ?> 
    <?php echo $brand ; ?>
<?php } ?>